<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer("buses_id")->unsigned()->nullable();
            $table->integer("vacation_id")->unsigned()->nullable();
            $table->string('event_name');
            $table->date('start_date');
            $table->date('end_date');
            $table->foreign("buses_id")->references("id")->on("buses");
            $table->foreign("vacation_id")->references("id")->on("vacations");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}

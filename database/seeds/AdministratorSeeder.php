<?php

use Illuminate\Database\Seeder;

class AdministratorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $administrator = new \App\User;
        $administrator->name = "administrator";
        $administrator->email = "ziyad@thareeq.me";
        $administrator->password = \Hash::make("adminseorangpujangga");
        $administrator->save();
        $this->command->info("Succesfully added");
    }
}

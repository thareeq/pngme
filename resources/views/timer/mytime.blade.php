@extends('layouts.global')
@section('title')
    Your time    
@endsection

@section('content')
    @if(session('status'))
    <div class="alert alert-success">
        {{session('status')}}
    </div>
    @endif 

    <div class="bg-white shadow-sm p-3">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="text-right" id="time"></h3>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer-scripts')
<script>
    var countDownDate = {{strtotime("$now->date $now->hour:$now->minutes:$now->seconds" )}} * 1000;
    var now = {{time()}} * 1000;
    var x = setInterval(function() {

    // Get todays date and time
    // 1. JavaScript
    // var now = new Date().getTime();
    // 2. PHP
    now = now + 1000;

    // Find the distance between now an the count down date
    var distance = countDownDate - now;

    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    // Output the result in an element with id="demo"
    document.getElementById("time").innerHTML = days + "d " + hours + "h " +
        minutes + "m " + seconds + "s ";

    // If the count down is over, write some text 
    if (distance < 0) {
        {{}}
        clearInterval(x);
        document.getElementById("time").innerHTML = "EXPIRED";
    }
    }, 1000);


</script>

@endsection

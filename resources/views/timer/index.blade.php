@extends('layouts.global')

@section('title')
    Setting timer
@endsection


@section('content')
    <div class="cold-md-7">
        <form method="POST" action="{{ route('timers') }}" enctype="multipart/form-data" class="p-3 mb-2 bg-white text-dark shadow" value >
            @csrf
            <input type="hidden" value="PUT" name="_method" >
            <label>Date</label>
            <input type="date" name="date" class="form-control">
            <br>

            <label>Hour</label>
            <input type="number" name="hour" id="hour" class="form-control" placeholder="Enter hours">
            <br>

            <label>Minute</label>
            <input type="number" name="minute" id="minute" class="form-control" placeholder="Enter Minute">
            <br>

            <label>Seconds</label>
            <input type="number" name="seconds" id="seconds" class="form-control" placeholder="Enter Seconds">
            <br>

            <input type="submit" value="save" class="btn btn-primary"> 
        </form>
    </div>
@endsection
@extends('layouts.global')
@section('title')
    Adding Bus
@endsection

@section('content')
    <form enctype="multipart/form-data" action="{{route('buses.store')}}" class="bg-white shadow-sm p-3"  method="POST">
            @csrf

            @if(session('status'))
                <div class="alert alert-success">
                    {{session('status')}}
                </div>
            @endif 
            
            <label>Bus Name</label><br>
            <input type="text" class="form-control" name="name">
            <br>
            
            <label>Description</label><br>
            <textarea class="ckeditor"  name="description" class="form-control"></textarea>
            <br>

            <label>Price</label><br>
            <input type="number" class="form-control" name="price">
            <br>

            <label>Bus Category</label>
            <select name="category" class="form-control">
                <option value="HIACE">HIAce</option>
                <option value="ELF">ELF</option>
            </select>
            <br>

            <label>image</label>
            <br>
            <input type="file" multiple class="form-control" name="image[]">
            <br>

            <label>Capacity </label>
            <br>
            <input type="number" class="form-control" name="capacity">
            <br>

            <label>Facilities</label><br>
            <textarea class="ckeditor"  name="facilities" class="form-control"></textarea>
            <br>

            <input class="btn btn-primary" type="submit" value="Save"/>

        </form>
@endsection

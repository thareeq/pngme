@extends('layouts.global')
@section('title')
    List Buses
@endsection

@section('content')
    @if(session('status'))
        <div class="alert alert-success">
            {{session('status')}}
        </div>
    @endif 

    <div class="row">
        <div class="col-md-8">
            <form action="{{route('buses.index')}}">
                <div class="input-group">
                    <input type="text" name="name" class="form-control" placeholder="Filter by category name" >
                    <div class="input-group-append">
                        <input type="submit" value="Filter" class="btn btn-primary">
                    </div>
                </div>
            </form>
        </div>

        <div class="col-md-4 text-right">
            <a href="{{route('buses.create')}}" class="btn btn-primary">Add Bus</a>
        </div>
    </div>

    <hr class="my-3">


    <table class="table table-hover">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Capacity</th>
                <th scope="col">category</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($buses as $bus)
                <tr>
                    <th scope="row">{{$bus->id}}</th>
                    <td>{{$bus->name}}</td>
                    <td>{{$bus->capacity}}</td>
                    <td>{{$bus->category}}</td>
                    <td>
                        <button type="submit" class="btn btn-success"  value="">
                            <a href="{{route('buses.edit', ['id' => $bus->id])}}"><i class="fas fa-edit"></i></a>
                        </button>

                        <form onsubmit="return confirm('Delete this user permanently?')" class="d-inline" action="{{route('buses.destroy', ['id' => $bus->id ])}}" method="POST">
                                @csrf
                                <input type="hidden" name="_method" value="DELETE">

                                <button type="submit" value="Delete" class="btn btn-danger" >
                                    <i class="far fa-trash-alt"></i> 
                                </button>
                                
                        </form>

                    
                    </td>
                </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <td colSpan="10">
                {{$buses->appends(Request::all())->links()}}
                </td>
            </tr>
        </tfoot>
    </table>
@endsection
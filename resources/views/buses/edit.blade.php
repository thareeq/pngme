@extends('layouts.global')
@section('title')
    Edit Bus
@endsection

@section('content')
    <form action="{{route('buses.update', ['id' => $bus->id])}}"  enctype="multipart/form-data" method="POST" class="bg-white shadow-sm p-3">
        @csrf
        <input type="hidden" value="PUT" name="_method">

        <label>Bus name</label><br>
        <input type="text" class="form-control" name="name" value="{{$bus->name}}">
        <br>

        <label>Bus Category</label>
        <select name="category" class="form-control">
            {{-- <option selected disabled value="{{$bus->category}}">{{$bus->category}}</option> --}}
            <option value="HIACE">HIAce</option>
            <option value="ELF">ELF</option>
        </select>
        <br>

        @if($bus->image)
            <span>Current image</span><br>
            <img src="{{asset('storage/'. $bus->image)}}" width="120px">
            <br><br>
        @endif

        <input
            type="file"
            class="form-control"
            name="image">
            <small class="text-muted">Let it if you wont change</small>
        <br><br>


        <label for="">Facilities</label>
        <br>
        <textarea name="facilities"  class="ckeditor" class="form-controll">{{$bus->facilities}}</textarea>
        <br>
        <input type="submit" value="Update" class="btn btn-primary">
    
    </form>
@endsection
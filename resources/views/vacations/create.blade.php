@extends('layouts.global')

@section('title')
    Create Trip
@endsection

@section('content')
    <form enctype="multipart/form-data" class="bg-white shadow-sm p-3"  action="{{route('vacations.store')}}" method="POST">
        @csrf

        @if(session('status'))
            <div class="alert alert-success">
                {{session('status')}}
            </div>
        @endif 

        <label>Destination</label><br>
        <input type="text" class="form-control" name="destination">
        <br>

        <label>Location</label><br>
        <textarea  name="location" id="location" class="form-control"></textarea>

        <br>

        <label>image</label>
        <br>
        <input type="file" class="form-control" name="image">
        <br>

    <label><b>Image Description</b></label>
        <br>
        <input type="file" multiple class="form-control" name="image_desc[]">
        <br>


        <label>Status</label>
        <select name="status" class="form-control">
            <option value="AVAILABLE">Tersedia</option>
            <option value="EXPIRED">Tidak Ada</option>
        </select>
        <br>

        <label>Facilities</label><br>
        <textarea class="ckeditor"  name="facilities" class="form-control"></textarea>
        <br>

        <label>Timeline</label><br>
        <textarea class="ckeditor"  name="timeline" class="form-control"></textarea>
        <br>

        <label>Term and Condition</label><br>
        <textarea class="ckeditor" name="terms" class="form-control"></textarea>
        <br>

        <input class="btn btn-primary" type="submit" value="Save"/>

    </form>
@endsection 
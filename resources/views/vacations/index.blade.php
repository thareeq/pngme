@extends('layouts.global')
@section('title')
    List Vacation
@endsection

@section('content')
    @if(session('status'))
        <div class="alert alert-success">
            {{session('status')}}
        </div>
    @endif 


    <div class="row">
        <div class="col-md-8">
            <form action="{{route('vacations.index')}}">
                <div class="input-group">
                    <input type="text" name="destination" class="form-control" placeholder="Filter by category name" >
                    <div class="input-group-append">
                        <input type="submit" value="Filter" class="btn btn-primary">
                    </div>
                </div>
            </form>
        </div>

        <div class="col-md-4 text-right">
            <a href="{{route('vacations.create')}}" class="btn btn-primary">Create Vacation</a>
        </div>
    </div>

    <hr class="my-3">


    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered table-stripped text-center">
                <thead>
                    <tr class="table-active text-center">
                        <th><b>Name</b></th>
                        <th><b>Slug</b></th>
                        <th><b>Location</b></th>
                        <th><b>Actions</b></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($vacations as $category)
                        <tr>
                            <td>{{$category->destination}}</td>
                            <td>{{$category->slug}}</td>
                            <td>
                                {{$category->location}}
                            </td>
                            <td >
                                <button type="submit" class="btn btn-success"  value="">
                                    <a href="{{route('vacations.edit', ['id' => $category->id])}}"><i class="fas fa-edit"></i></a>
                                </button>
                                

                                {{-- <a href="#" style="width: 50px;"><i class="far fa-trash-alt"></i></a> --}}
                                
                                <form onsubmit="return confirm('Delete this user permanently?')" class="d-inline" action="{{route('vacations.destroy', ['id' => $category->id ])}}" method="POST">
                                        @csrf
                                        <input type="hidden" name="_method" value="DELETE">

                                        <button type="submit" value="Delete" class="btn btn-danger" >
                                            <i class="far fa-trash-alt"></i> 
                                        </button>
                                        
                                        {{-- <input type="submit" value="Delete" class="btn btn-primary"> --}}
                                </form>

                                
                                
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colSpan="10">
                            {{$vacations->appends(Request::all())->links()}}
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

@endsection
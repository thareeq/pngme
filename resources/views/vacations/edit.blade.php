@extends('layouts.global')
@section('title')
    Edit Vacation
@endsection

@section('content')
    <form
        action="{{route('vacations.update', ['id' => $vacation->id])}}"
        enctype="multipart/form-data"
        method="POST"
        class="bg-white shadow-sm p-3">

        @csrf
        <input type="hidden" value="PUT" name="_method">

        <label>Destination</label><br>
        <input type="text" class="form-control" name="destination" value="{{$vacation->destination}}">
        <br>

        <label for="">Slug Destination</label>
        <input type="text" class="form-control" name="slug" value="{{$vacation->slug}}">
        <br>

        <label>Location</label><br>
        <input type="text" name="location" id="location" class="form-control" value="{{$vacation->location}}">

        <br>


        @if($vacation->image)
            <span>Current image</span><br>
            <img src="{{asset('storage/'. $vacation->image)}}" width="120px">
            <br><br>
        @endif

        <input
            type="file"
            class="form-control"
            name="image">
            <small class="text-muted">Let it if you wont change</small>
        <br><br>


        @if($vacation->status)
            <label>Status</label>
            
            <select name="status" class="form-control">
                <option>{{$vacation->status}}</option>
                <option value="AVAILABLE">Tersedia</option>
                <option value="EXPIRED">Tidak Ada</option>
            </select>
        @endif
        
        <br>

        <label>Facilities</label><br>
        <textarea class="ckeditor"    name="facilities" class="form-control">{{$vacation->facilities}}</textarea>
        <br>

        <label>Timeline</label><br>
        <textarea class="ckeditor"  name="timeline" class="form-control">{{$vacation->timeline}}</textarea>
        <br>

        <label>Term and Condition</label><br>
        <textarea class="ckeditor" name="terms" class="form-control">{{$vacation->terms}}</textarea>
        <br>

        <input class="btn btn-primary" type="submit" value="Save"/>
    </form>

@endsection
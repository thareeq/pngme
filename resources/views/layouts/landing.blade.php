<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Piknik and Go</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    

    <link rel="stylesheet" href="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="{{asset('vendor/font-awesome/css/font-awesome.min.css')}}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    <!-- BEGIN SHAREAHOLIC CODE -->
    <link rel='preload' href='https://apps.shareaholic.com/assets/pub/shareaholic.js' as='script' />
    <script data-cfasync="false" async src="https://apps.shareaholic.com/assets/pub/shareaholic.js" data-shr-siteid="b08fd96e1072eaac42ef5d624e93b7af"></script>
    <!-- END SHAREAHOLIC CODE -->


    <!-- Custom styles for this template -->

    <link href="{{asset('css/full-slider.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('vendor/animate/animate.css')}}">
  </head>

  <body onload="initMap()">

    <!-- Navigation -->

    <nav id="headers" class="navbar navbar-expand-lg navbar-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="/">
          <h2>Piknik and Go</h2>
          <!-- <img src="vendor/image/brand/piknikandgo.png" alt=""> -->
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive"
          aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="{{ route('tours') }}">Destinasi</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('bus') }}">Bus</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('contact') }}">Hubungi Kami</a>
            </li>
          </ul>
        </div>
        </div>
      </div>
    </nav>

    @yield("body")
  
    <!-- footer -->
    <footer id="footer">
      <div class="container">
        <div class="row">

          

              <div class="footer-menu-1 col-lg-6">
                <h1>Piknik and Go Travel</h1>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                </p>

                <a href="https://api.whatsapp.com/send?phone=6282130315537&text=Halo%20Admin%20Saya%20Mau%20Order%20Baju"><i class="fab fa-whatsapp"></i></a>
                <a href="https://www.instagram.com/dmctelkomuniv/" class="social-media"><i class="fab fa-instagram"></i></a> 
                <a href="#" class="social-media"><i class="fab fa-facebook-square"></i></a>
              </div>
          
          
            <div class="footer-menu-2 col-lg-6">
              <h2>Contact Information</h2>
              <div class="menu">
                  <i class="fas fa-map-marker-alt">&nbsp;Komplek Jerang Indonesia</i><br>
                  <i class="fas fa-phone-volume">&nbsp;+62 878-2095-2886</i><br>
                  <i class="far fa-envelope">&nbsp;piknikandgo@gmail.com</i>
              </div>
              
            </div>

        </div>
      </div>
    </footer>


    <!-- Bootstrap core JavaScript -->


    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.js"></script>
    <script type="text/javascript" src="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/owl.carousel.js"></script>

    <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/btn.js')}}"></script> 
    <script src="{{asset('js/dist/zoomify.js')}}"></script>
    <script src="{{asset('js/jquery.slicknav.min.js')}}"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js"></script>
    
    @yield("javascript")

  </body>
</html>
@extends('layouts.global')
@section('title')
    Timeline
@endsection


@section('content')
    <div class="col-md-12">
        <div class="p-3 mb-2 bg-white text-dark shadow">
           {!! $calendar_details->calendar() !!}
        </div>
    </div>
@endsection

@section('footer-scripts')
	{!! $calendar_details->script() !!}
@endsection


@extends('layouts.global')
@section('title')
    Timeline
@endsection

@section('content')

    <form enctype="multipart/form-data" class="bg-white shadow-sm p-3" action="{{ route('tasks.store') }}" method="post">
       @csrf
        <label for="">Task</label>
        <br />
        <input type="text" name="name" class="form-control" class="form-control" />
        <br />
        <label for="">Description</label>
        <br />
        <textarea name="description" class="form-control"></textarea>
        <br />
        <label for="">Start Time</label><br>
        <input type="text" name="task_date" class="date" />
        <br /><br />
        <input class="btn btn-primary" type="submit" value="Save"/>
    </form>
@endsection

@section('footer-scripts')
    <script>
    $('.date').datepicker({
        autoclose: true,
        dateFormat: "yy-mm-dd"
    });
</script>

@endsection
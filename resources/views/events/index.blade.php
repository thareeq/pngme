@extends('layouts.global')
@section('title')
    Schedule
@endsection
@section('content')
    @if(session('status'))
    <div class="alert alert-success">
        {{session('status')}}
    </div>
    @endif 

    <div class="row">
        <div class="col-md-8">
            <form action="{{route('buses.index')}}">
                <div class="input-group">
                    <input type="text" name="name" class="form-control" placeholder="Filter by category name" >
                    <div class="input-group-append">
                        <input type="submit" value="Filter" class="btn btn-primary">
                    </div>
                </div>
            </form>
        </div>

        <div class="col-md-4 text-right">
            <a href="{{route('events.create')}}" class="btn btn-primary">Add schedule</a>
        </div>
    </div>
    
    
    <hr class="my-3">

    <table class="table table-hover">
        <thead>
            <tr class="text-center">
                <th scope="col">#</th>
                <th scope="col">Name of events</th>
                <th scope="col">Bus</th>
                <th scope="col">Destination</th>
                <th scope="col">Start Date</th>
                <th scope="col">End Date</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($events as $event)
                <tr class="text-center">
                    <th scope="row" >{{$event->id}}</th>
                    <td>{{$event->event_name}}</td>
                    <td>{{$event->buses->name}}</td>
                    <td>{{$event->vacations->destination}}</td>
                    <td>{{$event->start_date}}</td>
                    <td>{{$event->end_date}}</td>
                    <td>
                        <button type="submit" class="btn btn-success"  value="">
                            {{-- {{route('buses.edit', ['id' => $bus->id])}} --}}
                            <a href=""><i class="fas fa-edit"></i></a>
                        </button>
{{-- 
                        <form onsubmit="return confirm('Delete this user permanently?')" class="d-inline" action="{{route('buses.destroy', ['id' => $bus->id ])}}" method="POST">
                                @csrf
                                <input type="hidden" name="_method" value="DELETE">

                                <button type="submit" value="Delete" class="btn btn-danger" >
                                    <i class="far fa-trash-alt"></i> 
                                </button>
                                
                        </form> --}}
                    
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

@endsection
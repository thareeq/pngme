@extends('layouts.global')
@section('title')
@endsection
@section('content')
   
    <div class="cold-md-7">
        <form method="POST" action="{{route('events.add')}}" enctype="multipart/form-data" class="p-3 mb-2 bg-white text-dark shadow" >
            <h3>Jadwal Pemesanan</h3>
            @csrf
            <br>
            

            <label for="kodesoal">Acara</label>
            <input class="form-control" type="text" name="event_name" id="kode" value="">
            <br>

            <label for="Bus">Bus</label><br>
            <select name="buses" id="bus" class="form-control">
            </select>
            <br><br/>
            
            <label for="destination">Destinasi</label><br>
            <select name="destination" class="form-control" data-target-vacation>
            </select>
            <br><br/>
            

            <label for="kodesoal">tanggal mulai</label>
            <input class="form-control" type="date" name="start_date" id="kode" value="">
            <br>

            <label for="kodesoal">tanggal berakhir</label>
            <input class="form-control" type="date" name="end_date" id="kode" value="">
            <br>
            
            <input class="btn btn-primary text-right" type="submit" value="Simpan">
        </form>
    </div>
    
@endsection

@section('footer-scripts')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

    <script>
        $('#bus').select2({
            ajax: {
            url: 'http://png.me/ajax/bus/search',
            processResults: function(data){
            return {
            results: data.map(function(item){return {id: item.id, text:item.name} })
            }
            }
            }
        });

        $('[data-target-vacation]').select2({
         ajax: {
         url: 'http://png.me/ajax/vacation/search',
         processResults: function(data){
         return {
         results: data.map(function(item){return {id: item.id, text:item.destination} })
            }
        }
         }
        });
    </script>


        
@endsection
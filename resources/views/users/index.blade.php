@extends('layouts.global')

@section('title')
    Profile User
@endsection
@section('content')
    <div class="cold-md-7">
        <form method="POST" action="#" enctype="multipart/form-data" class="p-3 mb-2 bg-white text-dark shadow" >
            <h3>Hello {{Auth::user()->name}}</h3>
            {{-- <h3>Hello </h3> --}}
            @csrf
            <br>
            

            <label for="kodesoal">Name</label>
            <input class="form-control" type="text" name="name"  value="{{$user->name}}">
            <br>

            <label >Email</label>
            <input class="form-control" type="text" name="email"  value="{{$user->email}}">
            <br>

            <label>Pasword</label>
            <input class="form-control" type="password" name="password_old"  value="{{$user->password}}">
            <br>

            <label>Change Password</label>
            <input class="form-control" type="password" name="password" >
            <br>

            <label>Password Confirmation</label>
            <input class="form-control" type="password" name="password_confirmation" >
            <br>
            
            
            <input class="btn btn-primary text-right" type="submit" value="Save">
        </form>
    </div>
    
@endsection
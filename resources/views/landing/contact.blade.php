@extends('layouts.landing')

@section('title')
    contact us
@endsection

@section('body')
    <header>
        <div class="jumbotron" style="background-image: url('vendor/image/tours/background/background.jpg'); background-size: cover; ">
            <h1 class="text-center" style="font-weight: bold; font-size: 50px; color: #fff; padding-top: 50px;">Hubungi Kami</h1>     
        </div>
    </header>

    <div class="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="contact_information">
                        <h1 class="text-center">Hubungi Kami</h1>
                        <br><br>
                        <div class="row">
                            <div class="col-md-10 offset-md-1">
                                <div class="contact_form">
                                    <form method="POST" action="{{route('contact.post')}}" enctype="multipart/form-data">
                                        @csrf
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label> <b>Nama</b></label><br>
                                                <input type="text" name="name" class="contact_form_name input_field">        
                                            </div>
                                            <div class="col-md-6">
                                                <label for=""><b>Email</b> </label><br>
                                                <input type="email" name="email" class="contact_form_name input_field">
                                            </div>
                                        </div>

                                        <br>
                                        <label for=""><b> Judul</b></label><br>
                                        <input type="text" name="title" class="contact_form_name input_field">
                                        <br><br>

                                        <label for=""><b>Deskripsi</b> </label><br>
                                        <textarea  name="description" class="contact_form_name input_field"></textarea>

                                        <button type="submit" class="form_submit_button button text-center" data-attach-loading>
                                            Kirim pesan
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="contact_location">
                        <div class="row">
                            <div class="col-lg-12">
                                {{-- <div id="map"></div> --}}
                                <div id="map-container-google-3" class="z-depth-1-half map-container-3">
                                    <iframe src="https://maps.google.com/maps?q=serang&t=k&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0"
                                        style="border:0" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



@section('javascript')
        <script>
            // Initialize and add the map
            function initMap() {
            // The location of Uluru
            var uluru = {lat: -6.116398, lng: 106.152519};
            // The map, centered at Uluru
            var map = new google.maps.Map(
                document.getElementById('map'), {zoom: 10, center: uluru});
            // The marker, positioned at Uluru
            var marker = new google.maps.Marker({position: uluru, map: map});
            }


        </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDp6FYYqXKwjn8AJKzka8OzQnGF2hnXIIw" type="text/javascript">
                $(function(){
                initMap();    
            });

    </script>
@endsection
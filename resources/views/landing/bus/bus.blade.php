@extends('layouts.landing')

@section('body')

    <header>
        <div class="jumbotron" style="background-image: url('vendor/image/PO/bisone.jpg'); background-size: cover; ">
            <h1 class="text-center" style="font-weight: bold; font-size: 50px; color: #fff; padding-top: 50px;">Armada</h1>     
        </div>
    </header>

    <div id="trip">
        <div class="destinasi">
            <div class="container">
                <div class="destinasi-information">
                        @foreach ($buses as $bus)
                        <div class="row">
                            <div class="col-md-5 ">
                                <div class="destinasi-image text-left">
                                    <div class="image-tours">
                                        @if($bus->image)
                                            <img src="{{ asset('storage/'.$bus->image) }}" width="70px"/>
                                       @else
                                            N/A
                                       @endif
                                        {{-- <img src="vendor/image/PO/bisfour.jpg" alt=""> --}}
                                    </div>
                                    <div class="offer_name"><a href="#">{{$bus->category}}</a></div>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="destinasi-content ">
                                    <h1>{{$bus->name}}</h1>
                                    <hr>
                                    <div class="content-information">
                                        <i class="fas fa-map-marked-alt" aria-hidden="true">&nbsp; Bandung</i> <br>
                                        <i class="fas fa-chair">&nbsp; {{$bus->capacity}} Seat</i>
                                    </div>
                                    <p class="caption">Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente consectetur totam itaque beatae consequuntur, dicta dolores asperiores sed fugit animi?</p>
                                    <a href="{{ route('bus.detail',['id' => $bus->id]) }}">
                                        <button class="button text-left">Selengkapnya</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <br><br>
                    @endforeach
                </div>

            </div>
        </div>
    </div>
    

    <button id="topbtn" class="scrolltotop"><i class="fas fa-arrow-up"></i></button>

@endsection
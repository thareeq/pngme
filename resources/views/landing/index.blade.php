@extends('layouts.landing')

@section('body')
    <header>
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
          <div class="carousel-item active" style="background-image: url('vendor/image/Slider/slide1.jpg')">

            <div class="carousel-caption d-none d-md-block">
              <h3>Travel  The World</h3>
              <p>This is a description for the first slide.</p>
              <a href="tours.html" class="pink-btn">View All Tours</a>
            </div>
          </div>
          <!-- Slide Two - Set the background image for this slide in the line below -->
          <div class="carousel-item" style="background-image: url('vendor/image/Slider/bus2.jpg')">
            <div class="carousel-caption d-none d-md-block">
              <h3>You Can Travell Anywhere</h3>
              <p>This is a description for the second slide.</p>
            </div>
          </div>
          <!-- Slide Three - Set the background image for this slide in the line below -->
          <div class="carousel-item" style="background-image: url('vendor/image/Slider/bus3.jpg')">
            <div class="carousel-caption d-none d-md-block">
              <h3>Feel Comfort With Us</h3>
              <p>This is a description for the third slide.</p>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </header>

    <!-- Logo -->
    <div class="container">
      <div class="row">
          <div class="section-header text-center">
              <h2 style="padding-top: 40px;">OUR SERVICES</h2>
          </div>


        <section id="logo">
          <div class="container text-center">
            <div class="row animated bounceInUp">
                <div class="col-md-6 col-lg-4">
                  <div class="box-services-hexagon">
                      <div class="hexagon-wrapper">
                          <div class="hexagon">
                              <img src="vendor/image/support/bus.png" width="120" height="120">
                          </div> 
                          <br>
                          <h4 class="box-services-hexagon-title"><a href="#">Order</a></h4>
                          <p class="box-services-hexagon-text">Our team of experts has years of experience in remodeling homes 
                              including kitchens, basements etc.</p> 
                      </div>
                  </div>
                </div>
                <div class="col-md-6 col-lg-4">
                  <div class="box-services-hexagon">
                    <div class="hexagon-wrapper">
                        <div class="hexagon">
                            <img src="vendor/image/support/trip.png" width="120" height="120">
                        </div> 
                        <br>
                        <h4 class="box-services-hexagon-title"><a href="tours.html">Tours</a></h4>
                        <p class="box-services-hexagon-text">Our team of experts has years of experience in remodeling homes 
                            including kitchens, basements etc.</p> 
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-lg-4">
                  <div class="box-services-hexagon">
                    <div class="hexagon-wrapper">
                        <div class="hexagon">
                            <img src="vendor/image/support/travelling.png" width="120" height="120">
                        </div> 
                        <br>
                        <h4 class="box-services-hexagon-title"><a href="#">Travelling</a></h4>
                        <p class="box-services-hexagon-text">Our team of experts has years of experience in remodeling homes 
                            including kitchens, basements etc.</p> 
                    </div>
                  </div>
                </div>
            </div>
          </div>
        </section>
      </div>
    </div>

    <!-- About Us -->
    <section id="about">
      <div class="container">
          <div class="section-header">
              <h2 style="color: #fff;">ABOUT US</h2>
          </div>
        <div class="row row-30">
          <div class="col-md-5">
            <div class="panel-group" id="accordion">
              <div class="panel panel-default">
                <div class="panel-heading animated bounce">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne">
                      <span><i class="fas fa-bus"></i></span> Loream Ipsum</a>
                  </h4>
                </div>

                <div id="collapseOne" class="panel-collapse collapse in show">
                  <div class="panel-body" id="body1">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard
                    dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen
                    book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially
                    unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and
                    more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </div>
                </div>
              </div>


              <div class="panel panel-default">
                <div class="panel-heading animated bounce">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapstwo">
                      <span> <i class="fas fa-bus"></i></span>Loream Ipsum I</a>
                  </h4>
                </div>
              
                <div id="collapstwo" class="panel-collapse collapse in">
                  <div class="panel-body" id="body2">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                    standard
                    dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type
                    specimen
                    book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining
                    essentially
                    unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                    and
                    more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </div>
                </div>
              </div>


              <div class="panel panel-default">
                <div class="panel-heading animated bounce">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapsethree">
                      <span><i class="fas fa-bus"></i> </span> Loream Ipsum II</a>
                  </h4>
                </div>
              
                <div id="collapsethree" class="panel-collapse collapse in">
                  <div class="panel-body" id="body3">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                    standard
                    dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type
                    specimen
                    book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining
                    essentially
                    unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                    and
                    more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </div>
                </div>
              </div>
            </div>


            </div>
          <div class="col-md-7">
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner text-right">
                <div class="carousel-item  active">
                  <img class="d-block w-100" src="vendor/image/PO/bisthree.jpg" alt="First slide">
                </div>
                <div class="carousel-item">
                  <img class="d-block w-100" src="vendor/image/PO/bisone.jpg" alt="Second slide">
                </div>
                <div class="carousel-item">
                  <img class="d-block w-100" src="vendor/image/PO/bisfive.jpg" alt="Third slide">
                </div>
              </div>
              <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
          </div>
        </div>
      </div>
    </section>

    <!-- Team Section -->
    <section id="team">
      <div class="container">
        <div class="section-header">
            <h2>Gallery</h2>
        </div>  
        <div class="row">
            <!-- <div class="team"> -->
              <div class="col-md-6 mg-b-20">
                <div class="post post-service text-right" id="bisone">
                  <img src="vendor/image/PO/bisone.jpg">
                </div>
              </div>

              <div class="col-md-6 mg-b-20">
                <div class="post post-service text-left" id="bistwo">
                    <img src="vendor/image/PO/bistwo.jpg">
                </div>
              </div>

              <div class="col-md-6 mg-b-20">
                <div class="post post-service text-right" id="bisthree">
                  <img src="vendor/image/PO/bisthree.jpg">
                </div>
              </div>
  
              <div class="col-md-6 mg-b-20">
                <div class="post post-service text-left" id="bisfour">
                    <img src="vendor/image/PO/bisfour.jpg">
                </div>
              </div>

              <div class="col-md-6 mg-b-20">
                <div class="post post-service text-right" id="bisfive">
                  <img src="vendor/image/PO/bisfive.jpg">
                </div>
              </div>
    
              <div class="col-md-6 mg-b-20">
                <div class="post post-service text-left" id="bissix">
                    <img src="vendor/image/PO/bissix.jpg">
                </div>
              </div>
            </div>
        </div>
      </div>
    </section>

    <button id="topbtn" class="scrolltotop"><i class="fas fa-arrow-up"></i></button>


    
@endsection


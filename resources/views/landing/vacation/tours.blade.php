@extends('layouts.landing')

@section('body')
    <header>
        <div class="jumbotron" style="background-image: url('vendor/image/tours/background/background.jpg'); background-size: cover; ">
            <h1 class="text-center" style="font-weight: bold; font-size: 50px; color: #fff; padding-top: 50px;">Trip</h1>     
        </div>
    </header>


    <div id="trip">
        <div class="destinasi">
            <div class="container">
                <div class="row">
                    
                    @foreach ($vacations as $vacation)
                        
                    
                        <div class="col-md-5 ">
                            <div class="destinasi-image text-left">
                                <div class="image-tours">
                                    @if($vacation->image)
                                        <img src="{{ asset('storage/'.$vacation->image) }}" width="70px"/>
                                   @else
                                        N/A
                                   @endif
                                </div>
                                <div class="offer_name"><a href="#">{{ $vacation->location }}</a></div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="destinasi-content ">
                                <h1>{{ $vacation->destination }}</h1>
                                <hr>
                                <div class="rating">
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star"></span>
                                </div>
                                <div class="content_price"><span class="mg-0 pd-0">Rp.</span> {{ $vacation->price }}</div>
                                <div class="destionation-status">Available</div>
                                <p class="caption">Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente consectetur totam itaque beatae consequuntur, dicta dolores asperiores sed fugit animi?</p>
                                <a href="{{ route('tours.detail',['id' => $vacation->id]) }}">
                                    <button class="button text-left">Selengkapnya</button>
                                </a>
                            </div>
                        </div>
                    @endforeach
                    <div class="text-center">
                        {{$vacations->appends(Request::all())->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    

    <button id="topbtn" class="scrolltotop"><i class="fas fa-arrow-up"></i></button>
    
@endsection
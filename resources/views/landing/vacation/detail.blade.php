@extends('layouts.landing')

@section('body')
    <header>
        <div class="jumbotron" style="background-image: url('vendor/image/tours/background/background.jpg'); background-size: cover; ">
            <h1 class="text-center" style="font-weight: bold; font-size: 50px; color: #fff; padding-top: 50px;">Trip</h1>     
        </div>
    </header>

    <div class="listing">
        {{-- single listing --}}
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="single_listing">
                        <div class="row">
                            <div class="col-md-10 offset-md-1">
                                {{-- vacation info --}}
                                <div class="hotel-info">
                                    {{-- title --}}
                                    <div class="hotel_title_container d-flex flex-lg-row flex-column">
                                        <div class="hotel_title_content">
                                            <h1 class="hotel_title">
                                                Lorem, ipsum.
                                            </h1>
                                            <div class="rating">
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star"></span>
                                            </div>
                                            <div class="hotel_location">
                                                Lorem ipsum dolor sit.
                                            </div>
                                        </div>
                                    </div>

                                    {{-- Listing image --}}
                                    <div class="hotel_image">
                                        <div class="row">
                                            <img src=" https://explorebantentravel.com/storage/app/uploads/public/5c0/8e4/4c2/thumb_20_1200x650_0_0_crop.jpg" >
                                            <div class="hotel_review_container d-flex flex-column align-items-center justify-content-center">
                                                <div class="hotel_review">
                                                    <div class="hotel_review_content">
                                                        <div class="hote_review_title">
                                                            Sangat Baik
                                                        </div>
                                                        <div class="hotel_review_subtitle">
                                                            0 reviews
                                                        </div>
                                                    </div>
                                                    <div class="hotel_review_rating text-center">
                                                        0
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    {{-- hotel gallery --}}
                                    <div class="hotel_gallery">
                                        <div class="hotel_slider_container">
                                            <div class="owl-carousel owl-theme hotel_slider owl-loaded owl-drag">
                                                <div class="item"><img src="https://explorebantentravel.com/storage/app/uploads/public/5c1/2dd/f0d/thumb_52_250x250_0_0_crop.jpg" alt=""></div>
                                                <div class="item"><h4>2</h4></div>
                                                <div class="item"><h4>3</h4></div>
                                                <div class="item"><h4>4</h4></div>
                                                <div class="item"><h4>5</h4></div>
                                                <div class="item"><h4>6</h4></div>
                                                <div class="item"><h4>7</h4></div>
                                                <div class="item"><h4>8</h4></div>
                                                <div class="item"><h4>9</h4></div>
                                                <div class="item"><h4>10</h4></div>
                                                <div class="item"><h4>11</h4></div>
                                                <div class="item"><h4>12</h4></div>
                                            </div>
                                        </div>
                                    </div>

                                    

                                    <div class="hotel_info_text">
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Veniam amet voluptatum perspiciatis aliquid quisquam harum id, laborum iure, ipsum facilis neque autem qui? Sint ipsum earum dignissimos quidem minus officia!
                                        Obcaecati sequi itaque ullam eaque fugiat minima in! Dolore, praesentium labore. Ipsum quidem repellat ipsa consectetur praesentium debitis ratione sapiente doloremque minima? Consequuntur id fuga, est vel mollitia aliquam in!
                                        Voluptatum sapiente reiciendis possimus eveniet assumenda praesentium nostrum quidem placeat? Optio ex ut molestias consectetur quae numquam hic reiciendis! Corrupti dolorem dolore cum, unde laudantium ducimus aut porro veritatis eum!
                                        <hr>
                                    </div>
                                    
                                    <a href="https://www.shareaholic.com/api/share/?v=1&apitype=1&apikey=8943b7fd64cd8b1770ff5affa9a9437b&service=5&title=The%20Hottest%20VC%20No%20One%20Has%20Ever%20Heard%20Of&link=http://www.robgo.org/post/376467064/the-hottest-vc-no-one-has-ever-heard-of&shortener=google&source=Shareaholic&" target="_blank">Post to Facebook</a>
                                </div>
                            </div>
                        </div>
                        <br><br>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="terms mg-t-40">
                                    <ul class="nav nav-tabs nav-justified" id="mytTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="facility-tab" data-toggle="tab" href="#facility" role="tab" aria-controls="facility" aria-selected="true">Fasilitas</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="itinerary-tab" data-toggle="tab" href="#itinerary" role="tab" aria-controls="itinerary" aria-selected="false">Itineraries</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="terms-tab" data-toggle="tab" href="#terms" role="tab" aria-controls="terms" aria-selected="false">Terms and Conditions</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="review-tab" data-toggle="tab" href="#review" role="tab" aria-controls="terms" aria-selected="false">Review</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content pd-t-30" id="myTabContent">
                                        <div class="tab-pane fade show active" id="facility" role="tabpanel" aria-labelledby="facility-tab" >
                                            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Iusto praesentium saepe placeat neque labore ipsam, recusandae est minima? Doloremque porro eius perspiciatis vero quis ea aliquam obcaecati nam in dicta.
                                        </div>
                                        <div class="tab-pane fade" id="itinerary" role="tabpanel" aria-labelledby="itinerary-tab" >
                                            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Iusto praesentium saepe placeat neque labore ipsam, recusandae est minima? Doloremque porro eius perspiciatis vero quis ea aliquam obcaecati nam in dicta.
                                        </div>
                                        <div class="tab-pane fade" id="terms" role="tabpanel" aria-labelledby="terms-tab" >
                                            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Iusto praesentium saepe placeat neque labore ipsam, recusandae est minima? Doloremque porro eius perspiciatis vero quis ea aliquam obcaecati nam in dicta.
                                        </div>
                                        <div class="tab-pane fade" id="review" role="tabpanel" aria-labelledby="review-tab" >
                                            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Iusto praesentium saepe placeat neque labore ipsam, recusandae est minima? Doloremque porro eius perspiciatis vero quis ea aliquam obcaecati nam in dicta.
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <div class="button button_line_1 button_phone trans_200">
                                    <a href="tel:081319126648" class="trans_200">
                                        <i class="fa fa-mobile"></i> TELEFON
                                    </a>
                                </div>
                                <div class="button button_line_1 button_whatsapp trans_200">
                                    <a href="https://wa.me/+082130315537?text=mau bertanya soal Explore Krakatau" class="trans_200">
                                        <i class="fa fa-whatsapp"></i> WHATSAPP
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

@endsection
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Auth::routes();


Route::match(["GET", "POST"], "/register", function(){
 return redirect("/login");
})->name("register");

Route::get('/user/profile', 'UserController@index');

// Generating view
    Route::get('/tours', 'ViewController@tourview')->name('tours');
    Route::get('/tours/detail/{id}', 'ViewController@tourdetail')->name('tours.detail');

    Route::get('/', 'ViewController@index');

    Route::get('/bus', 'ViewController@busview')->name('bus');
    Route::get('/bus/details/{id}', 'ViewController@busdetail')->name('bus.detail');
    
    Route::get('/contact', 'ViewController@contactview')->name('contact');
    Route::post('/contact', 'ViewController@store')->name('contact.post');


Route::group(['middleware' => 'auth'], function(){
   
    // Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('vacations', 'VacationController');
    Route::resource('buses', 'BusController');
    Route::resource('tasks', 'TasksController'); //creating timeline

    // creating bus
    Route::get('/home', 'EventsController@schedule')->name('events.schedule');
    Route::get('/ajax/bus/search','EventsController@ajaxSearchbus'); 
    Route::get('/ajax/vacation/search','EventsController@ajaxSearchvacation');
    Route::get('/events', 'EventsController@index')->name('events');
    Route::get('events/create', 'EventsController@create')->name('events.create');
    Route::post('events/create', 'EventsController@addEvent')->name('events.add');

    // setting timer
    Route::get('timer', 'TimerController@index')->name('timer');
    Route::put('timer/update', 'TimerController@update')->name('timers');
    Route::get('timer/running', 'TimerController@mytime')->name('timer.mytime');

    // profile and setting
    Route::get('profile', 'UserController@index')->name('user.profile');
    Route::get('setting', 'UserController@seting')->name('user.setting');
});


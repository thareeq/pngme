$(document).ready(function(){
    $(window).scroll(function(){
        if($(this).scrollTop()>100){
          $('#topbtn').fadeIn();
        }
        else{
          $('#topbtn').fadeOut();
        }
    });
    
    $("#topbtn").click(function(){
        $('html, body').animate({scrollTop : 0}, 800);
    });

    $('.section-padding').owlCarousel({
        loop: true,
        autoloop: 3000,
        margin: 10,
        nav: true,
        responsive: {
          0: {
            items: 1
          },
          600: {
            items: 3
          },
          1000: {
            items: 5
          }
        }
      });
  
  
    $('.owl-carousel').owlCarousel({
      loop:true,
      margin:10,
      responsiveClass:true,
      responsive:{
          0:{
              items:1,
              nav:true
          },
          600:{
              items:3,
              nav:false
          },
          1000:{
              items:5,
              nav:true,
              loop:false
          }
      }
  });
    
    $(function () {
      var navbar = $('.navbar');
  
      $(window).scroll(function () {
        if ($(window).scrollTop() <= 40) {
          navbar.removeClass('navbar-scroll');
        } else {
          navbar.addClass('navbar-scroll');
        }
      });
    });
  
    $(function ()
    {
        $('#logo').on('click', function ()
        {
            $(this).width(1000);
        });
    });
  
    $('#bistwo').zoomify({
      duration: 500,
      easing:   'linear',
      scale:    0.9
    });
  
    $('#bisone').zoomify({
      duration: 500,
      easing:   'linear',
      scale:    0.9
    });
  
    $('#bisfour').zoomify({
      duration: 500,
      easing:   'linear',
      scale:    0.9
    });
  
    $('#bisthree').zoomify({
      duration: 500,
      easing:   'linear',
      scale:    0.9
    });
  
    $('#bisfive').zoomify({
      duration: 500,
      easing:   'linear',
      scale:    0.9
    });
  
    $('#bissix').zoomify({
      duration: 500,
      easing:   'linear',
      scale:    0.9
    });
  
    $('.panel-heading a').click(function() {
      $('.panel-heading').removeClass('active');
      if(!$(this).closest('.panel').find('.panel-collapse').hasClass('in'))
          $(this).parents('.panel-heading').addClass('active');
    });
  
  
  
    $("#collapstwo").click(function(){
      $("#body1").hide(1000);
    });

    $('.owl-carousel').owlCarousel({
      stagePadding: 50,
      loop:true,
      margin:10,
      nav:true,
      responsive:{
          0:{
              items:1
          },
          600:{
              items:3
          },
          1000:{
              items:5
          }
      }
  })


    var modal = document.getElementById('login-form');

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }


    // ===============================================
    // Preloader
    // ===============================================
    jQuery(window).load(function() { // makes sure the whole site is loaded
        $('#status').fadeOut(); // will first fade out the loading animation
        $('#preloader').delay(100).fadeOut('slow'); // will fade out the white DIV that covers the website.
        $('body').delay(100).css({'overflow':'visible'});
    })

    
});
<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Calendar;
use Auth;
use Validator;
use App\Events;



class EventsController extends Controller
{

    public function index(Request $request){
        $event = \App\Events::with(['buses','vacations'])->paginate(10);

        $filter =  $request->get('name');

        if($filter){
            $event = \App\Events::where("event_name", "LIKE","%$filter%")->paginate(10);
        }

        return view('events.index',['events' => $event]);
    }
    public function schedule(){
        $events = Events::get();
        $event_list = [];
        foreach ($events as $key => $event) {
            $event_list[] = Calendar::event(
                $event->event_name,
                true,
                new \DateTime($event->start_date),
                new \DateTime($event->end_date)
                
            );
        }

        $calendar_details = Calendar::addEvents($event_list);
        return view('home', compact('calendar_details'));
    }
    
    public function create(){
        return view('events.create');
    }

    public function addEvent(Request $request){
        $validator = Validator::make($request->all(),[
            'event_name' => 'required',
            'start_date' => 'required',
            'end_date' => 'required'
        ]);

        if ($validator->fails()){
            \Session::flash('warning','Please enter the valid details');
            return redirect::to('/events')->withInput()->withErrors($validator);
        }

        $events = new Events;
        $name =  $request->get('event_name');
        $events->event_name = $name;
        $events->start_date = $request['start_date'];
        $events->end_date = $request['end_date'];
        $events->save();
    
        // return var_dump((int)$request->get('buses'));
        $events->buses()->attach((int)$request->get('buses'));
        $events->vacations()->attach((int)$request->get('destination'));
        return redirect()->route('events.index')->with('status', 'Pemesanan telah dibuat');
    }

    public function ajaxSearchbus(Request $request){
        $keyword = $request->get('q');
        $bus = \App\Bus::where("name", "LIKE", "%$keyword%")->get();
        return $bus;
    }

    public function ajaxSearchvacation(Request $request){   
        $keyword = $request->get('destination');
        $tes = \App\Vacation::where("destination", "LIKE", "%$keyword%")->get();
        return $tes;
    }
}

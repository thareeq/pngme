<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class VacationController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $vacation = \App\Vacation::paginate(10);
        $filter =  $request->get('destination');

        if($filter){
            $vacation = \App\Vacation::where("destination", "LIKE","%$filter%")->paginate(10);
        }
        return view('vacations.index', ['vacations' => $vacation]);
    }


    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vacations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $destination = $request->get('destination');
        $new_vacation = new \App\Vacation;

        $new_vacation->destination = $destination;
        $new_vacation->location = $request->get('location');
        $new_vacation->facilities = $request->get('facilities');
        $new_vacation->timeline = $request->get('timeline');
        $new_vacation->status = $request->get('status');
        $new_vacation->terms = $request->get("terms");
        $new_vacation->slug = str_slug($destination, '-');

        if($request->file('image')){
            $file = $request->file('image')->store('images-destination', 'public');
            $new_vacation->image = $file;
        } 

        $pathImages = [];

        if(count($request->file('image_desc'))) {
            foreach($request->file('image_desc') as $img) {
                $file = $img->store('images-destination', 'public');
                array_push($pathImages, $file);
            }
        }

        $new_vacation->image_description = json_encode($pathImages);
        $new_vacation->save();
        return redirect()->route('vacations.index')->with('status', 'trip successfully created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vacation = \App\Vacation::findOrFail($id);
        return view('vacations.show', ['vacations' => $vacation]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = \App\Vacation::findOrFail($id);
        return view('vacations.edit',['vacation' => $edit]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $vacation = \App\Vacation::findOrFail($id);
        $destination =  $request->get('destination');
        $vacation->destination = $destination;
        $vacation->location = $request->get('location');
        $vacation->facilities = $request->get('facilities');
        $vacation->timeline = $request->get('timeline');
        $vacation->status = $request->get('status');
        $vacation->terms = $request->get("terms");
        $vacation->slug = str_slug($destination, '-');

        if($request->file('image')){
            if($vacation->image && file_exists(storage_path('app/public/' . $category->image))){
                \Storage::delete('public/' . $vacation->destination);
            }

            $new_image = $request->file('image')->store('images-destination', 'public');
            $vacation->image = $new_image;
        }

        $vacation->save();
        
        return redirect()->route('vacations.index', ['id' => $id])->with('status', 'vacation successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vacation = \App\Vacation::findOrFail($id);
        $vacation->delete();
        return redirect()->route('vacations.index')->with('status', 'Category successfully moved to trash');

    }
}

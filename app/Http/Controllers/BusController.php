<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BusController extends Controller
{
    /**
     * Display a listing of the resource.
     *x
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $bus = \App\Bus::paginate(10);
        $filter =  $request->get('name');

        if($filter){
            $bus = \App\Bus::where("name", "LIKE","%$filter%")->paginate(10);
        }
        return view('buses.index', ['buses' => $bus]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('buses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $bus = new \App\Bus;
        $name = $request->get('name');
        $bus->name = $name;
        $bus->slug = str_slug($name, '-');
        $bus->description = $request->get('description'); 
        $bus->category = $request->get('category');
        $bus->price = $request->get('price');
        $bus->capacity = $request->get('capacity');
        $bus->facilities = $request->get('facilities');

        // if($request->file('image')){
        //     $file = $request->file('image')->store('bus', 'public');
        //     $bus->image = $file;
        // } 

        $pathImages = [];

        if(count($request->file('image'))) {
            foreach($request->file('image') as $img) {
                $file = $img->store('bus', 'public');
                array_push($pathImages, $file);
            }
        }

        $bus->image = json_encode($pathImages);
        //var_dump($request->file('image'));
        //return;

        $bus->save();
        return redirect()->route('buses.index')->with('status', 'Bus successfully created');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = \App\Bus::findOrFail($id);
        return view('buses.edit',['bus' => $edit]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $bus = \App\Bus::findOrFail($id);
        $name =  $request->get('name');
        $bus->name = $name;
        $bus->category = $request->get('category');
        $bus->slug = str_slug($name, '-');

        if($request->file('image')){
            if($bus->image && file_exists(storage_path('app/public/' . $bus->image))){
                \Storage::delete('public/' . $bus->name);
            }

            $new_image = $request->file('image')->store('images-destination', 'public');
            $bus->image = $new_image;
        }

        $bus->save();
        
        return redirect()->route('buses.index', ['id' => $id])->with('status', 'bus successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bus = \App\Bus::findOrFail($id);
        $bus->delete();
        return redirect()->route('buses.index')->with('status', 'Bus deleted from list');
    }

    

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LandingController extends Controller
{
    public function index(){
        return view('landing/index');
    }

    public function tours(){
        return view('landing/tours');
    }

    public function bus(){
        return view('landing/bus');
    }

    public function contact(){
        return view('landing/contact');
    }
}
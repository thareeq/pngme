<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

class ViewController extends Controller
{

    public function index(){
        return view('landing.index');
    }

    public function tourview(){
        $vacation = \App\Vacation::paginate(5);
        return view('landing.vacation.tours', ['vacations' => $vacation]);
    }

    public function tourdetail($id){
        $tour = \App\Vacation::findOrFail($id);
        return view('landing.vacation.detail', ['vacations' => $tour]);
    }

    public function busview(){
        $bus = \App\Bus::paginate(5);
        return view('landing.bus.bus', ['buses' => $bus]);
    }

    public function busdetail($id){
        $bus = \App\Bus::findOrFail($id);
        return view('landing.bus.detail', ['bus' => $bus]);
    }


    

    public function contactview(){
        return view('landing.contact');
    }

    public function store(Request $request){
        $validation = \Validator::make($request->all(),[
            "name" => "required|min:5|max:100",
            "email" => "required|email|unique:users",
            "title" => "required|min:5|max:100",
            "description" => "required"
        ])->validate();

        $contact = new \App\Contacts;
        $contact->name = $request->get('name');
        $contact->email = $request->get('email'); 
        $contact->title = $request->get('title');
        $contact->description = $request->get('description');
        $contact->save(); 
        return redirect()->route('contact');

    }
}

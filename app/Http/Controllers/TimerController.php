<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Facades\jpmurray\LaravelCountdown\Countdown;

class TimerController extends Controller
{
    public function index(){
        return view('timer.index');
    }

    public function mytime(){
        // date_default_timezone_set('');
        $timer = \App\Time::findOrFail(1);
      
        // $hour = $request->get('hour');
        // $minute = $request->get('minute');
        // $second = $request->get('seconds');
        // $target = Carbon::createFromTime($hour, $minute, $second);
        // $now = Carbon::now();
        
        // $diff = $now->diffInMinutes($target, false);
        // $ds = date('H:i:s', mktime(0,$diff));

        // $silit = Carbon::createFromFormat('Y-m-d H', $diff)->toDateTimeString();
        // return var_dump($diff->isoFormat('LLLL'));
        
        // convertToHoursMins($diff);
        
        
        return view('timer.mytime', ['now' => $timer]);
    }

    public function update(Request $request){
        $timer = \App\Time::findOrFail(1);
        $timer->date = $request->get('date');
        $timer->hour = $request->get('hour');
        $timer->minutes = $request->get('minute');
        $timer->seconds = $request->get('seconds');
        $timer->save(); 
        return redirect()->route('timer.mytime')->with('status', 'Time updated');
    }
}

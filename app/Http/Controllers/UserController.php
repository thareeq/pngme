<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    // ,['id' => Auth::user()->id]) 
    public function index(){
        $id = \Auth::user()->id;
        $user = \App\User::findOrFail($id);
        return view('users.index', ['user' => $user]);
    }
}

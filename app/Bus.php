<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bus extends Model
{
    protected $fillable = ['name','slug', 'price', 'description','category','facilities','capacity','image','updated_at', 'created_at'];

    public function events(){
        return $this->belongsToMany("App\Events");
    }
}

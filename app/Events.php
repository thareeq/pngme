<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Events extends Model
{ 
  
    // protected $fillable = ['slug','buses_id', 'vacation_id', 'event_name','start_date','end_date'];

    public function buses(){
        return $this->belongsToMany('App\Bus','buses_id');
    }
       
    
    public function vacations(){
        return $this->belongsToMany('App\Vacation','vacation_id');
    }
}
